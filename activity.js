let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

const {name, bday, age, isEnrolled, classes} = student1

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}

const {name1, bday1, age1, isEnrolled1, classes1} = student2

const introduce = (student) => {

	//Note: You can destructure objects inside functions.
 
	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
}

const getCube = (num) => Math.pow(num,3)

let cube = getCube(2);

console.log(cube)

let numArr = [15,16,32,21,21,2]

numArr.forEach(loopz = (num) =>{

	console.log(num);
})

let numsSquared = numArr.map(sq = (num) => num ** 2)


console.log(numsSquared);

class dog {
	constructor(name, breed, dogAge){
		this.name = name
		this.breed = breed
		this.dogAge = dogAge *7
	}
}

let dog1 = new dog("Brown", "Poodle", 1)
let dog2 = new dog("Black", "Labrador", 2)

console.log(dog1)
console.log(dog2)